package com.amg.fakebot

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class MessageAdapter : ListAdapter<Message, RecyclerView.ViewHolder>(DiffCallbacks) {

    companion object DiffCallbacks : DiffUtil.ItemCallback<Message>() {
        override fun areItemsTheSame(oldItem: Message, newItem: Message): Boolean {
            return oldItem.type == newItem.type
        }

        override fun areContentsTheSame(oldItem: Message, newItem: Message): Boolean {
            return oldItem == newItem
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        if (viewType == TypeMessage.QUESTION.ordinal) {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_question, parent, false)
            return QuestionViewHolder(view)
        } else {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_answer, parent, false)
            return AnswerViewHolder(view)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val message = getItem(position)

        if (getItemViewType(position) == TypeMessage.QUESTION.ordinal) {
            holder as QuestionViewHolder
            holder.bind(message)
        } else {
            holder as AnswerViewHolder
            holder.bind(message)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return getItem(position).type
    }

    inner class QuestionViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val tvQuestion = view.findViewById<TextView>(R.id.tv_question)

        fun bind(message: Message) {
            tvQuestion.text = message.message
        }
    }

    inner class AnswerViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val tvAnswer = view.findViewById<TextView>(R.id.tv_answer)

        fun bind(message: Message) {
            tvAnswer.text = message.message
        }
    }

}