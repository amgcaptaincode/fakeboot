package com.amg.fakebot

data class Message(val type: Int, val message: String)