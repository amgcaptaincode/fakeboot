package com.amg.fakebot

import android.app.Activity
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.amg.fakebot.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var adapter: MessageAdapter
    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MessageViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(this).get(MessageViewModel::class.java)

        binding.rvMessages.layoutManager = LinearLayoutManager(this)
        adapter = MessageAdapter()
        binding.rvMessages.adapter = adapter

        viewModel.messagesLiveData.observe(this, Observer { messagesValue ->
            if (messagesValue.isNotEmpty()) {
                binding.rvMessages.visibility = VISIBLE
                binding.tvNoMessagesYet.visibility = GONE
                adapter.submitList(messagesValue)
                adapter.notifyDataSetChanged()
                binding.rvMessages.scrollToPosition(messagesValue.size - 1)
            } else {
                binding.rvMessages.visibility = GONE
                binding.tvNoMessagesYet.visibility = VISIBLE
            }
        })

        binding.ivSend.setOnClickListener {
            sendMessages()
        }

    }

    private fun sendMessages() {
        val message = binding.edMessage.text.toString()
        if (message.isEmpty()) {
            Toast.makeText(this, "Debes insertar un mensaje para enviarlo", Toast.LENGTH_SHORT)
                .show()
        } else {
            viewModel.addQuestion(message)
            binding.edMessage.text.clear()
            hideKeyboard()

            val runnable = Runnable {
                viewModel.addResponse()
            }

            Handler(Looper.getMainLooper()).postDelayed(runnable, 800)

        }
    }

    private fun Activity.hideKeyboard() {
        hideKeyboard(currentFocus ?: View(this))
    }

    private fun Context.hideKeyboard(view: View) {
        val inputMethodManager =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }
}