package com.amg.fakebot

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MessageViewModel : ViewModel() {

    private var _messagesLiveData: MutableLiveData<MutableList<Message>> = MutableLiveData()
    val messagesLiveData: LiveData<MutableList<Message>>
        get() = _messagesLiveData

    private var messageAnswer = mutableListOf<Message>()

    init {
        _messagesLiveData.value = mutableListOf()
        initAnswer()
    }

    fun addQuestion(question: String) {
        _messagesLiveData.value?.add(Message(TypeMessage.QUESTION.ordinal, question))
        _messagesLiveData.notifyObserver()
    }

    fun addResponse() {
        _messagesLiveData.value?.add(getAnswer())
        _messagesLiveData.notifyObserver()
    }

    private fun initAnswer() {
        messageAnswer.add(Message(TypeMessage.ANSWER.ordinal, "Si"))
        messageAnswer.add(Message(TypeMessage.ANSWER.ordinal, "No"))
        messageAnswer.add(Message(TypeMessage.ANSWER.ordinal, "Pregunta de nuevo"))
        messageAnswer.add(Message(TypeMessage.ANSWER.ordinal, "Es muy probale"))
        messageAnswer.add(Message(TypeMessage.ANSWER.ordinal, "No lo creo"))
        messageAnswer.add(Message(TypeMessage.ANSWER.ordinal, "No sé 😅"))
        messageAnswer.add(
            Message(
                TypeMessage.ANSWER.ordinal,
                "Más respuestas que quieras agregar de tu repertorio"
            )
        )
    }

    private fun getAnswer(): Message {
        val indexRandom = (0 until messageAnswer.size).random()
        return messageAnswer[indexRandom]
    }

}