package com.amg.fakebot

enum class TypeMessage(value: Int) {
    QUESTION(1),
    ANSWER(2)
}